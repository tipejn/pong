﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPaddle : MonoBehaviour {

    public Ball theBall;

    public float speed = 30;

    public float lerpTweek = 2f;

    private Rigidbody2D rigidBody;

	// Use this for initialization
	void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (theBall.transform.position.y > transform.position.y)
        {
            ReturnTheBallUp();
        }
        else if (theBall.transform.position.y < transform.position.y)
        {
            ReturnTheBallDown();
        }
        else
        {
            ReturnTheBallNeutral();
        }
    }

    void ReturnTheBallNeutral()
    {
        SetRigidBodyVelocity(0, 0);
    }

    void ReturnTheBallDown()
    {
        SetRigidBodyVelocity(0, -1);
    }

    void ReturnTheBallUp()
    {
        SetRigidBodyVelocity(0, 1);
    }

    void SetRigidBodyVelocity(float xPos, float yPos)
    {
        Vector2 dir = new Vector2(xPos, yPos).normalized;
        rigidBody.velocity = Vector2.Lerp(rigidBody.velocity, dir * speed, lerpTweek * Time.deltaTime);
    }
}
